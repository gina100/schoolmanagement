﻿using System.Web;
using System.Web.Optimization;

namespace SchoolManagement
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }

        //public static void HomePage(BundleCollection bundles)
        //{
        //    bundles.Add(new ScriptBundle("~/js").Include(
        //              "~/js/jquery.min.js", "~/js/bootstrap.js", "~/js/easing.js", "~/js/jquery.min.js",
        //              "~/js/jquery-ui.js", "~/js/jquery.vide.min.js", "~/js/move-top.js", "~/js/smoothbox.jquery2.js"));

        //    bundles.Add(new StyleBundle("~/css").Include(
        //              "~/css/bootstrap.css",
        //              "~/css/style.css", "~/css/jquery-ui.css", "~/css/smoothbox.css"));
        //}
    }
}
