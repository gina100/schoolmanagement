﻿using Microsoft.Owin;
using Owin;
using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Google;
using SchoolManagement.Model;
using Microsoft.AspNet.Identity.EntityFramework;

[assembly: OwinStartupAttribute(typeof(SchoolManagement.Startup))]
namespace SchoolManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //CreateRolesandUsers();
        }
    }
}
//        private void CreateRolesandUsers()
//        {
//            ApplicationDbContext context = new ApplicationDbContext();

//            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
//            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

//            // creating Creating Manager role     
//            if (!roleManager.RoleExists("Parent"))
//            {
//                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
//                role.Name = "Parent";
//                roleManager.Create(role);

//            }

//            // creating Creating Employees role     
//            if (!roleManager.RoleExists("Employees"))
//            {
//                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
//                role.Name = "Employees";
//                roleManager.Create(role);

//            }
//            if (!roleManager.RoleExists("Students"))
//            {
//                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
//                role.Name = "Students";
//                roleManager.Create(role);

//            }
//        }
//    }
//}


    
