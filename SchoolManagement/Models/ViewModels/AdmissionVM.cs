﻿using SchoolManagement.Model.Entity;
using SchoolManagement.Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models.ViewModels
{
    public class AdmissionVM
    {
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Admissions Date")]
        [DataType(DataType.DateTime)]

        public DateTime AdmissionDate = DateTime.Now;
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Previous Schools Name")]
        public string PreviousSchool { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Previous Schools Address")]
        public string PreviousSchoolAddrs { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Previous Schools Document")]
        public byte[] PreviousSchoolDocument { get; set; }
        [Required(ErrorMessage = "Required!")]
        public string Extension { get; set; }

        [Required(ErrorMessage = "Required!")]
        public int SessionId { get; set; }
        [Required(ErrorMessage = "Required!")]
        public virtual Session Session { get; set; }

        [Required, DisplayName("Student Class")]
        public int StudentClassId { get; set; }

        public virtual StudentClass StudentClass { get; set; }


        [Required(ErrorMessage = "Required!")]
        public int GroupId { get; set; }

        public virtual Group Group { get; set; }

        [Required(ErrorMessage = "Required!")]
        public int ClassFeeId { get; set; }

        public virtual ClassFee ClassFee { get; set; }

        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Students's First Name(s)")]
        public string StudentName { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Students's Surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Father's full Name(s)")]
        public string FatherName { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Mother's Name Full Name(s)")]
        public string MotherName { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Date of Birth")]
        [DataType(DataType.DateTime)]
        public DateTime DateOfBirth { get; set; }
        [Required(ErrorMessage = "Required!")]
        public Race Race { get; set; }
        [Required(ErrorMessage = "Required!")]
        public HomeLanguage HomeLanguage { get; set; }
        [Required(ErrorMessage = "Required!")]
        [EmailAddress]
        [Display(Name = "Students Email")]
        public string StudentEmail { get; set; }

        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Present Address")]
        public string PresentAddress { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Parmanent Address")]
        public string ParmanentAddress { get; set; }
        [Required(ErrorMessage = "Required!")]
        public Religion Religion { get; set; }

        public byte[] Image { get; set; }
        public Gender Gender { get; set; }


        [Display(Name = "Guardians Name")]
        [Required(ErrorMessage = "Required!")]
        public string GuardianName { get; set; }

        [Display(Name = "Guardians Phone")]
        [Required(ErrorMessage = "Required!")]
        public int GuardianPhone { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Guardians Email")]
        [EmailAddress]
        public string GuardianEmail { get; set; }

        [Display(Name = "National Id Card Number")]
        [Required(ErrorMessage = "Required!")]
        public string NID { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Guardians Type")]

        public int GuardianTypeId { get; set; }
        public virtual GuardianType GuardianType { get; set; }
    }
}