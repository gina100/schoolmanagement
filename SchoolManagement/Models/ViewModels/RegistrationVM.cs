﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SchoolManagement.Model.Enum;
using System.Data.Entity;
using SchoolManagement.Model.Entity;

namespace SchoolManagement.Model.ViewModels
{
    public class RegistrationVM
    {
        [Display(Name = "Registration Date")]
        [DataType(DataType.DateTime)]

        public DateTime RegistrationDate = DateTime.Now;

        [Display(Name = "Previous Schools Name")]
        public string PreviousSchool { get; set; }

        [Display(Name = "Previous Schools Address")]
        public string PreviousSchoolAddrs { get; set; }

        [Display(Name = "Previous Schools Document")]
        public byte[] PreviousSchoolDocument { get; set; }

        public string Extension { get; set; }

        [Required]
        public int SessionId { get; set; }

        public virtual Session Session { get; set; }

        [Required]
        public int StudentClassId { get; set; }

        public virtual StudentClass StudentClass { get; set; }


        [Required]
        public int GroupId { get; set; }

        public virtual Group Group { get; set; }

        [Required]
        public int ClassFeeId { get; set; }

        public virtual ClassFee ClassFee { get; set; }

        [Required]
        [Display(Name = "Students's Name")]
        public string StudentName { get; set; }

        [Display(Name = "Father's Name")]
        public string FatherName { get; set; }

        [Display(Name = "Mother's Name")]
        public string MotherName { get; set; }

        [Display(Name = "Date of Birth")]
        [DataType(DataType.DateTime)]
        public DateTime DateOfBirth { get; set; }
        public Race Race { get; set; }

        public HomeLanguage HomeLanguage { get; set; }

        [EmailAddress]
        [Display(Name = "Students Email")]
        public string StudentEmail { get; set; }


        [Display(Name = "Present Address")]
        public string PresentAddress { get; set; }

        [Display(Name = "Parmanent Address")]
        public string ParmanentAddress { get; set; }

        public Religion Religion { get; set; }

        public byte[] Image { get; set; }
        public Gender Gender { get; set; }


        [Display(Name = "Guardians Name")]
        [Required]
        public string GuardianName { get; set; }

        [Display(Name = "Guardians Phone")]
        [Required]
        public int GuardianPhone { get; set; }

        [Display(Name = "Guardians Email")]
        [EmailAddress]
        public string GuardianEmail { get; set; }

        [Display(Name = "National Id Card Number")]
        [Required]
        public string NID { get; set; }

        [Display(Name = "Guardians Type")]
        public int GuardianTypeId { get; set; }
        public virtual GuardianType GuardianType { get; set; }
    }
}