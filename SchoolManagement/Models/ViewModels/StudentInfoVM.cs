﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace SchoolManagement.Model.ViewModels
{
    public class StudentInfoVM
    {
        [Required(ErrorMessage = "Required!")]
        public string StudentName { get; set; }
        [Required(ErrorMessage = "Required!")]
        public string FatherName { get; set; }
        [Required(ErrorMessage = "Required!")]
        public string MotherName { get; set; }
        [Required(ErrorMessage = "Required!")]
        public DateTime? BirtDate { get; set; }
        [Required(ErrorMessage = "Required!")]
        public string Session { get; set; }
        [Required(ErrorMessage = "Required!")]
        public string Shift { get; set; }
        [Required(ErrorMessage = "Required!")]
        public string Section { get; set; }
        [Required(ErrorMessage = "Required!")]
        public string GroupName { get; set; }
        [Required(ErrorMessage = "Required!")]
        public string ClassName { get; set; }
        [Required(ErrorMessage = "Required!")]
        public string Roll { get; set; }

       

    }
}