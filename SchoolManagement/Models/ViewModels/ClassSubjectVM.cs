﻿using SchoolManagement.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models.ViewModels
{
    public class ClassSubjectVM
    {
        //public ICollection<ClassName> ClassNames { get; set; }
        //public ICollection<Subject> Subjects;
        public ICollection<SubjectVM> SubjectVM;
    }
}