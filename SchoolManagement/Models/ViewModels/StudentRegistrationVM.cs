﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagement.Model.ViewModels
{
    public class StudentRegistrationVM
    {
        public ICollection<StudentInfoVM> StudentInfoVM { get; set; }
        public ICollection<RegistrationVM> RegistrationVM { get; set; }
    }
}