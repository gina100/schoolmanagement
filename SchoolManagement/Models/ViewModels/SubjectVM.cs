﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models.ViewModels
{
    public class SubjectVM
    {
        public string ClassSubjectCode { get; set; }
        public string ClassSubjectName { get; set; }

    }
}