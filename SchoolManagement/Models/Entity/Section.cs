﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolManagement.Model.Entity
{
    public class Section
    {
     [Key]
        public int Id { get; set; }

        [Required,DisplayName("Section")]
        public string Name { get; set; }
    }
}