﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SchoolManagement.Model.Enum;
using System.ComponentModel.DataAnnotations;

namespace SchoolManagement.Model.Entity
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Required!")]
        public string Name { get; set; }

        [Display(Name = "Surname")]
        public string Surname { get; set; }

       
        public Gender Gender { get; set; }

        [Display(Name = "Date of Birth")]
        public DateTime DOB { get; set; }

        public string MaritalStatus { get; set; }
        public Religion Religion { get; set; }

        public string Nationality { get; set; }

         [Display(Name = "National Id No.")]
        public string NID { get; set; }

        [Display(Name = "Present Address")]
        public string PresentAddress  { get; set; }

        [Display(Name = "Parmanent Address")]
        public string PermanentAddress { get; set; }

        [Required(ErrorMessage = "Required!")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Required!")]
        [EmailAddress]
        public string Email { get; set; }

        public byte[] Image { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }
        
        public ICollection<EmployeeEducation> EmployeeEducation { get; set; }
        public ICollection<EmploymentHistory> EmploymentHistory { get; set; }

        public ICollection<JobInfo> JobInfo { get; set; }
    }
}