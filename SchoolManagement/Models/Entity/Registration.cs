﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace SchoolManagement.Model.Entity
{
    public class Registration
    {
        [Key]
       public int Id { get; set; }
        public int SessionId { get; set; }
        public Session Session { get; set; }
        public int StudentClassId { get; set; }
        public StudentClass StudentClass { get; set; }
        public int StudentId { get; set; }
        public Student Student { get; set; }
    }

}