﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SendGrid.Helpers.Mail;

namespace SchoolManagement.Models.Entity
{
    public class Email
    {
        //overload with what you want to show in the email.
        public void SendConfirmation(string email, string date, int ClassName, string Status, string eventss, double total)
        {
            try
            {
                var myMessage = new SendGridMessage
                {
                    From = new EmailAddress("no-reply@homify.co.za", "Nqabakazulu Comphrehesive High School")
                };

                myMessage.AddTo(email);
                string subject = "Booking: ";
                string body = (
                    "Dear " + email + "<br/>"
                    + "<br/>"
                    + "Your Booking details are as follows: "
                    + "<br/>"
                    + "<br/>" + "Booking Date: " + Status
                    + "<br/>" + "Venue Booked: " + date
                    + "<br/>" + "Event Booked: " + eventss
                    + "<br/>" + "Number Of People: " + ClassName
                    + "<br/>" + "Total Price:" + " R" + total +
                    "<br/>" +
                    "<br/>" +
                    "<br/>" +


                    "We hope to hear from you soon." +
                    "<br/>" +
                    "Bright-Ideas");

                myMessage.Subject = subject;
                myMessage.HtmlContent = body;

                var transportWeb = new SendGrid.SendGridClient("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");

                transportWeb.SendEmailAsync(myMessage);
            }
            catch (Exception x)
            {
                Console.WriteLine(x);
            }

        }


    }
}
    
