﻿using Microsoft.Ajax.Utilities;
using SchoolManagement.Model;
using SchoolManagement.Model.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolManagement.Models.Entity
{
    public class AssignTeacheToClass
    {
      [Key]
        public int TeacherClassId { get; set; }
        public int TeacherId { get; set; }
        public virtual Teacher Teacher { get; set; }
        public int StudentClassId { get; set; }
        public virtual StudentClass StudentClass { get; set; }

        ApplicationDbContext db = new ApplicationDbContext();
        public bool CheckExists()
        {
            bool result = false;
            var dbRecord = db.AssignTeacheToClasses;
            foreach (var item in dbRecord)
            {
                if(item.TeacherId==TeacherId && item.StudentClassId == StudentClassId)
                {
                    result = true;
                }
            }
            return result;
        }

    }
}