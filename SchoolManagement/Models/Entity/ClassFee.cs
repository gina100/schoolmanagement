﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolManagement.Model.Entity
{
    public class ClassFee
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Fee Amount")]
        //[DataType(DataType.Currency)]
        public decimal AdmissionFee { get; set; }
        //[Required]
        //[Display(Name = "Fee Type")]

        //public  int FeeTypeId { get; set; }
        //public FeeType FeeType { get; set; }
        [Required]
        [Display(Name = "Class")]
        public int ClassNameId { get; set; }
        public virtual ClassName ClassName { get; set; }
    }
}