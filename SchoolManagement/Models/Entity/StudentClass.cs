﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolManagement.Model.Entity
{
    public class StudentClass
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int ClassNameId { get; set; }
        public virtual ClassName ClassName { get; set; }
      
        public int SectionId { get; set; }
        public Section Section { get; set; }

        public int ShiftId { get; set; }

        public Shift Shift { get; set; }
        private ApplicationDbContext db = new ApplicationDbContext();

        public string GetName()
        {
            var f = (from c in db.ClassNames
                     where ClassNameId == c.Id
                     select c.Name).FirstOrDefault();
            return f ;


        }


    }

}