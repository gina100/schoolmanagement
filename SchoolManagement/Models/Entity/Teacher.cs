﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SchoolManagement.Model.Entity;
using SchoolManagement.Model.Enum;

namespace SchoolManagement.Models.Entity
{
    public class Teacher
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public virtual Gender Gender { get; set; }

        public string Address { get; set; }


        public string Email { get; set; }

        [DisplayName("Phone Number")]
        public string PhoneNumber { get; set; }

        [DisplayName("Designations")]
        public int DesignationId { get; set; }
        public virtual Designation Designation { get; set; }

        [DisplayName("Class Name")]
       public int StudentClassId { get; set; }
        public StudentClass StudentClass { get; set; }



        //[Display(Name = "Credit to be Taken")]
        //public double CreditTaken { get; set; }
        //public double CreditLeft { get; set; }



    }
}