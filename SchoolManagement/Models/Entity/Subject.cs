﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolManagement.Model.Entity
{
    public class Subject
    {
        private int _total = 0;
        [Key]
        public int Id { get; set; }

        [Required,DisplayName("Subject Name")]
        public string Name { get; set; }

        
        [Required, DisplayName("Subject Code")]
        public string Code { get; set; }
        public double Subject_Credit { get; set; }

        public string SubjectAssignTo { get; set; }
        public int Theory { get; set; }
        public int Mcq { get; set; }
        public int Practical { get; set; }

        public int Total
        {
            get //get method for returning value
            {
                _total = this.Theory + this.Mcq + this.Practical;
                return _total;
            }
           private set // set method for storing value in name field.
            {
                _total = value;
            }
        }

        internal static object Where(Func<object, object> p)
        {
            throw new NotImplementedException();
        }

        public int StudentClassId { get; set; }

        public  StudentClass StudentClass { get; set; }

    }
}