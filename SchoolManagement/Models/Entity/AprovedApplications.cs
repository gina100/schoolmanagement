﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SchoolManagement.Model.Enum;
using SchoolManagement.Model.Entity;

namespace SchoolManagement.Models.Entity
{
    public class AprovedApplications
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Students's Full Name(s)")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Students's Surname")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Father's Full Name(s)")]
        public string FatherName { get; set; }
        [Required(ErrorMessage = "Required!")]

        [Display(Name = "Mother's Full Name(s)")]
        public string MotherName { get; set; }
        [Required(ErrorMessage = "Date fo Birth is required"), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime DOB { get; set; }
        [Required, DisplayName("National Identinty Number")]
        public string NID { get; set; }
        [Required(ErrorMessage = "You must select a gender")]

        public Gender Gender { get; set; }
        [Required(ErrorMessage = "You must select your Home Language")]
        public HomeLanguage HomeLanguage { get; set; }
        public Race Race { get; set; }
        [Required(ErrorMessage = "Required!")]
        [EmailAddress]
        public string Email { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Present Address")]
        public string PresentAddress { get; set; }
        [Required(ErrorMessage = "Required!")]
        [Display(Name = "Parmanent Address")]
        public string ParmanentAddress { get; set; }
        [Required(ErrorMessage = "Required!")]
        public Religion Religion { get; set; }





        [Required, DisplayName("Class Applying for")]
        public int ClassNameId { get; set; }
        public ClassName ClassName { get; set; }
        public string Status { get; set; }

    }
}