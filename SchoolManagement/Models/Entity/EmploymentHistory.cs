﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SchoolManagement.Model.Entity;
using System.ComponentModel.DataAnnotations;

namespace SchoolManagement.Model.Entity
{
    public class EmploymentHistory
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Schools Name")]
        [Required(ErrorMessage = "Required!")]
        public string SchoolName { get; set; }

        public string SchoolLocation { get; set; }

        [Display(Name = "Designations")]
        [Required(ErrorMessage = "Required!")]
        public string Designation { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime From { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime To { get; set; }

        public int EmployeeId { get; set; }
        public Employee Employee { get; set; }

        
    }
}