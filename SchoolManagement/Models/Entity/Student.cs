﻿using SchoolManagement.Model.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using System.ComponentModel.DataAnnotations.Schema;

using System.Linq;
using System.Web;

namespace SchoolManagement.Model.Entity
{
    public class Student
    {
        [Key]
        public int Id { get; set; }

        [Display(Name = "Students's Name")]
        public string Name { get; set; }
      
        [Display(Name = "Students's Surname")]
        public string Surname { get; set; }
        
        [Display(Name = "Father's Full Name(s)")]
        public string FatherName { get; set; }
        

        [Display(Name = "Mother's Name Full Name(s)")]
        public string MotherName { get; set; }

        
        [Display(Name = "Date of Birth")]

       
        [Required(ErrorMessage = "Date fo Birth is required"), DataType(DataType.Date), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]

        public DateTime? DateOfBirth { get; set; }

        public Gender Gender { get; set; }

        [Display(Name = "Home Language")]
        public HomeLanguage HomeLanguage { get; set; }
       
        public Race Race { get; set; }
        
        [EmailAddress]
        public string Email { get; set; }
       
        [Display(Name = "Present Address")]
        public string PresentAddress { get; set; }
       
        [Display(Name = "Parmanent Address")]
        public string ParmanentAddress { get; set; }
        
        public Religion Religion { get; set; }
        [Required(ErrorMessage = "Required!")]
        public byte[] Image { get; set; }
      
        [Display(Name = "User Name")]
        public string UserName { get; set; }


        //public virtual ICollection<Guardian> Guardians { get; set; }
        //public virtual ICollection<Registration> Registrations { get; set; }
    }
}