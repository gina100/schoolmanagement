﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using SchoolManagement.Model.Entity;
using SchoolManagement.Model;

namespace SchoolManagement.Models.Entity
{
    public class SubjectAssignToTeacher
    {
       [Key]
        public int Id { get; set; }

        [Required]
        public int StudentClassId { get; set; }
        public virtual StudentClass StudentClass { get; set; }
        [Required]
        public int TeacherId { get; set; }
        public virtual Teacher Teacher { get; set; }
        [Required]
        public int SubjectId { get; set; }
        public virtual Subject Subject { get; set; }
        ApplicationDbContext db = new ApplicationDbContext();
        public bool CheckExists()
        {
            bool result = false;
            var dbRecord = db.SubjectAssignToTeachers;
            foreach (var item in dbRecord)
            {
                if (item.SubjectId == SubjectId && item.StudentClassId == StudentClassId)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}