﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace SchoolManagement.Model.Entity
{
    public class Attendance
    {
        [Key]
        public int Id { get; set; }
        [Required, DisplayName ("Attendance Name")]
        public string Name { get; set; }
        public DateTime Attendance_Date { get; set; }
        public Boolean Attendance_status { get; set; }
        public int StudentClassId { get; set; }
        public virtual StudentClass StudentClass { get; set; }
       

    }
}