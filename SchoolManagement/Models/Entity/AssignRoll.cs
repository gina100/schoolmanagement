﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SchoolManagement.Model.Entity
{
    public class AssignRoll
    {
        [Key]
        public int Id { get; set; }
        [Required,DisplayName("Roll Number")]
        public string Roll { get; set; }

        public int SessionId { get; set; }

        public virtual Session Session { get; set; }

        public int StudentClassId { get; set; }

        public virtual StudentClass StudentClass { get; set; }

        public int StudentId { get; set; }

        public virtual Student Student { get; set; }
    }
}