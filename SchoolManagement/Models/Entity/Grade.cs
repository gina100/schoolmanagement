﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SchoolManagement.Model.Entity
{
    public class Grade
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [DisplayName("Grade Name")]
        public string Name { get; set; }
        [Required]
        [DisplayName("Lowest Mark")]
        public int LowestMark { get; set; }
        [Required]
        [DisplayName("Highest Mark")]
        public int HighestMark { get; set; }

    }
}