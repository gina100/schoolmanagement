﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagement.Model.Entity
{
    public class Enrollment
    {
        public int Id { get; set; }

        public int Roll { get; set; }
    }
}