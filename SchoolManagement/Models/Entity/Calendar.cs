﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolManagement.Model.Entity
{
    public class Calender
    {
        [Key]
        public int id { get; set; }
        public string Name { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
    }
}