﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SchoolManagement.Model.Entity
{
    public class School
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Schools Name")]
        public string Name { get; set; }

        [Display(Name = "Schools Address")]
        [Required]
        public string Address { get; set; }

        [Display(Name = "Schools Phone Primary")]
        [Required]
        public string PhonePrimary { get; set; }

        [Display(Name = "Schools Phone Alt")]
        public string PhoneAlt { get; set; }

        [Display(Name = "Schools Fax")]
        public string Fax { get; set; }

        [Display(Name = "Schools Email")]
        public string Email { get; set; }

        [Display(Name = "Logo")]
        public byte[] Logo { get; set; }
    }
}