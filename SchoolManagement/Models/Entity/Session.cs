﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SchoolManagement.Model.Entity
{
    public class Session
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [DisplayName("Session Name")]
        public string Name { get; set; }
    }
}