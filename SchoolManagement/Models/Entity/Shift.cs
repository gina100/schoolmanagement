﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace SchoolManagement.Model.Entity
{
    public class Shift
    {
        [Key]
        public int Id { get; set; }

        [Required,DisplayName("Shift")]
        public string Name { get; set; }
    }
}