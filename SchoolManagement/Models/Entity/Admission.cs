﻿using SchoolManagement.Model;
using System;
using SchoolManagement.Helper;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace SchoolManagement.Model.Entity
{
    public class Admission
    {
        [Key]
        public int Id { get; set; }

        [Required,DisplayName("Admission Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? AdmissionDate { get; set; }
        [Required,DisplayName("Previous school Name")]
        public string PreviousSchool { get; set; }
        //[Required(ErrorMessage = "Required!")]
        public string PreviousSchoolAddrs { get; set; }
        //[Required(ErrorMessage = "Required!")]
        public byte[] PreviousSchoolDocument { get; set; }

        public string Extension { get; set; }

        [Required(ErrorMessage = "Required!")]
        public int SessionId { get; set; }

        public virtual Session Session { get; set; }

        [Required,DisplayName( "Student Class")]
        public int StudentClassId { get; set; }

        public virtual StudentClass StudentClass { get; set; }
        [Required(ErrorMessage = "Required!")]
        public int? GroupId { get; set; }
        
        public virtual Group Group { get; set; }
        [Required(ErrorMessage = "Required!")]
        public int StudentId { get; set; }

        public virtual Student Student { get; set; }
        public int ClassFeeId { get; set; }
        public virtual ClassFee ClassFee { get; set; }

        //private ApplicationDbContext db = new ApplicationDbContext();

        //public string GetFee()
        //{
        //    var f = (from c in db.ClassFees
        //             where ClassFeeId == c.Id
        //             select c.AdmissionFee).FirstOrDefault();
        //    return f.ToString();

           
        //}
        
        
            
        }
        


    }

   
