﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SchoolManagement.Model.Entity;
using SchoolManagement.Models.Entity;

namespace SchoolManagement.Model
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        static ApplicationDbContext()
        {
            // Set the database intializer which is run once during application start
            // This seeds the database with admin user credentials and admin role
            Database.SetInitializer<ApplicationDbContext>(new ApplicationDbInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public DbSet<GuardianType> GuardianTypes { get; set; }
        public DbSet<Guardian> Guardians { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Admission> Admissions { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Section> Sections { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Shift> Shifts { get; set; }
        public DbSet<ExamMark> ExamMarks { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<StudentClass> StudentClasses { get; set; }
        public DbSet<ClassName> ClassNames { get; set; }
        public DbSet<AssignRoll> AssignRolls { get; set; }
        public DbSet<Designation> Designations { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<EducationLevel> EducationLevels { get; set; }
        public DbSet<ExamTitle> ExamTitles { get; set; }
        public DbSet<EmployeeEducation> EmployeeEducations { get; set; }
        public DbSet<EmploymentHistory> EmploymentHistorys { get; set; }
        public DbSet<JobInfo> JobInfos { get; set; }
        public DbSet<ClassFee> ClassFees { get; set; }
        public DbSet<AccountGroup> AccountGroups { get; set; }
        public DbSet<AccountList> AccountLists { get; set; }
        public DbSet<DefaultSetting> DefaultSettings { get; set; }
        public DbSet<School> Schools { get; set; }
        public DbSet<Calender> Calenders { get; set; }
        public DbSet<Registration> Registrations { get; set; }
        public DbSet<SubjectAssignToTeacher> SubjectAssignToTeachers { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<AssignTeacheToClass> AssignTeacheToClasses { get; set; }



        public DbSet<StudentApplication> Studentapplications { get; set; }

        public DbSet<FeeType> FeeTypes { get; set; }
        //public DbSet<IdentityRole> IdentityRole { get; set; }

        public DbSet<Grade> Grades { get; set; }
        public DbSet<Admin> Admins { get; set; }
        // public DbSet<Application_Information> Application_Information { get; set; }

        public DbSet<ApplicationDocuments> ApplicationDocuments { get; set; }

        public DbSet<AprovedApplications> AprovedApplications { get; set; }
        //public DbSet<SMS> SMS { get; set; }
        //Role Management
        //public DbSet<IdentityRole> IdentityRole { get; set; }
        //public DbSet<ApplicationUser> ApplicationUser { get; set; }
        //public DbSet<ApplicationRoleManager> appRoles { get; set; }
    }
}