﻿namespace SchoolManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplicationSchoolDbContextmdf : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountGroups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AccountLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                        OpeningBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrentBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AccountGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AccountGroups", t => t.AccountGroupId, cascadeDelete: true)
                .Index(t => t.AccountGroupId);
            
            CreateTable(
                "dbo.Admins",
                c => new
                    {
                        AdminID = c.String(nullable: false, maxLength: 128),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                    })
                .PrimaryKey(t => t.AdminID);
            
            CreateTable(
                "dbo.Admissions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AdmissionDate = c.DateTime(nullable: false),
                        PreviousSchool = c.String(nullable: false),
                        PreviousSchoolAddrs = c.String(),
                        PreviousSchoolDocument = c.Binary(),
                        Extension = c.String(),
                        SessionId = c.Int(nullable: false),
                        StudentClassId = c.Int(nullable: false),
                        GroupId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                        ClassFeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassFees", t => t.ClassFeeId, cascadeDelete: true)
                .ForeignKey("dbo.Groups", t => t.GroupId, cascadeDelete: true)
                .ForeignKey("dbo.Sessions", t => t.SessionId, cascadeDelete: true)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .ForeignKey("dbo.StudentClasses", t => t.StudentClassId, cascadeDelete: true)
                .Index(t => t.SessionId)
                .Index(t => t.StudentClassId)
                .Index(t => t.GroupId)
                .Index(t => t.StudentId)
                .Index(t => t.ClassFeeId);
            
            CreateTable(
                "dbo.ClassFees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AdmissionFee = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ClassNameId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassNames", t => t.ClassNameId, cascadeDelete: true)
                .Index(t => t.ClassNameId);
            
            CreateTable(
                "dbo.ClassNames",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        FatherName = c.String(),
                        MotherName = c.String(),
                        DateOfBirth = c.DateTime(),
                        HomeLanguage = c.Int(nullable: false),
                        Race = c.Int(nullable: false),
                        Email = c.String(),
                        PresentAddress = c.String(),
                        ParmanentAddress = c.String(),
                        Religion = c.Int(nullable: false),
                        Image = c.Binary(nullable: false),
                        Gender = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StudentClasses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ClassNameId = c.Int(nullable: false),
                        SectionId = c.Int(nullable: false),
                        ShiftId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassNames", t => t.ClassNameId, cascadeDelete:false )
                .ForeignKey("dbo.Sections", t => t.SectionId, cascadeDelete: true)
                .ForeignKey("dbo.Shifts", t => t.ShiftId, cascadeDelete: true)
                .Index(t => t.ClassNameId)
                .Index(t => t.SectionId)
                .Index(t => t.ShiftId);
            
            CreateTable(
                "dbo.Sections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Shifts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ApplicationDocuments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApplicationDate = c.DateTime(),
                        StudentApplicationId = c.Int(nullable: false),
                        PreviousSchool = c.String(nullable: false),
                        PreviousSchoolAddrs = c.String(nullable: false),
                        PreviousSchoolDocument = c.Binary(nullable: false),
                        Certificate = c.Binary(nullable: false),
                        CertifiedID = c.Binary(nullable: false),
                        HomeAddress = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StudentApplications", t => t.StudentApplicationId, cascadeDelete: true)
                .Index(t => t.StudentApplicationId);
            
            CreateTable(
                "dbo.StudentApplications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        FatherName = c.String(nullable: false),
                        MotherName = c.String(nullable: false),
                        DOB = c.DateTime(nullable: false),
                        NID = c.String(nullable: false),
                        Gender = c.Int(nullable: false),
                        HomeLanguage = c.Int(nullable: false),
                        Race = c.Int(nullable: false),
                        Email = c.String(nullable: false),
                        PresentAddress = c.String(nullable: false),
                        ParmanentAddress = c.String(nullable: false),
                        Religion = c.Int(nullable: false),
                        ClassNameId = c.Int(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassNames", t => t.ClassNameId, cascadeDelete: true)
                .Index(t => t.ClassNameId);
            
            CreateTable(
                "dbo.AprovedApplications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Surname = c.String(nullable: false),
                        FatherName = c.String(nullable: false),
                        MotherName = c.String(nullable: false),
                        DOB = c.DateTime(nullable: false),
                        NID = c.String(nullable: false),
                        Gender = c.Int(nullable: false),
                        HomeLanguage = c.Int(nullable: false),
                        Race = c.Int(nullable: false),
                        Email = c.String(nullable: false),
                        PresentAddress = c.String(nullable: false),
                        ParmanentAddress = c.String(nullable: false),
                        Religion = c.Int(nullable: false),
                        ClassNameId = c.Int(nullable: false),
                        Status = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassNames", t => t.ClassNameId, cascadeDelete: true)
                .Index(t => t.ClassNameId);
            
            CreateTable(
                "dbo.AssignRolls",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Roll = c.String(),
                        SessionId = c.Int(nullable: false),
                        StudentClassId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sessions", t => t.SessionId, cascadeDelete: true)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .ForeignKey("dbo.StudentClasses", t => t.StudentClassId, cascadeDelete: true)
                .Index(t => t.SessionId)
                .Index(t => t.StudentClassId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.AssignTeacheToClasses",
                c => new
                    {
                        TeacherClassId = c.Int(nullable: false, identity: true),
                        TeacherId = c.Int(nullable: false),
                        StudentClassId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TeacherClassId)
                .ForeignKey("dbo.StudentClasses", t => t.StudentClassId, cascadeDelete: true)
                .ForeignKey("dbo.Teachers", t => t.TeacherId, cascadeDelete: true)
                .Index(t => t.TeacherId)
                .Index(t => t.StudentClassId);
            
            CreateTable(
                "dbo.Teachers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        Gender = c.Int(nullable: false),
                        Address = c.String(),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        DesignationId = c.Int(nullable: false),
                        StudentClassId = c.Int(nullable: false),
                        CreditTaken = c.Double(nullable: false),
                        CreditLeft = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Designations", t => t.DesignationId, cascadeDelete: true)
                .ForeignKey("dbo.StudentClasses", t => t.StudentClassId, cascadeDelete: false)
                .Index(t => t.DesignationId)
                .Index(t => t.StudentClassId);
            
            CreateTable(
                "dbo.Designations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Calenders",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        start_date = c.DateTime(nullable: false),
                        end_date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.DefaultSettings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Vat = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SMSBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SMSStatus = c.Boolean(nullable: false),
                        From = c.DateTime(nullable: false),
                        To = c.DateTime(nullable: false),
                        Language = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EducationLevels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EducationLevelNaame = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmployeeEducations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EducationLevelId = c.Int(nullable: false),
                        ExamTitleId = c.Int(nullable: false),
                        Major = c.String(nullable: false),
                        InstituteName = c.String(nullable: false),
                        ResultType = c.Int(nullable: false),
                        CGPA = c.Single(nullable: false),
                        Scale = c.Int(nullable: false),
                        Marks = c.Single(nullable: false),
                        PassingYear = c.String(nullable: false),
                        Duration = c.Int(nullable: false),
                        Achievement = c.String(),
                        EmployeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EducationLevels", t => t.EducationLevelId, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .ForeignKey("dbo.ExamTitles", t => t.ExamTitleId, cascadeDelete: true)
                .Index(t => t.EducationLevelId)
                .Index(t => t.ExamTitleId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Surname = c.String(),
                        Gender = c.Int(nullable: false),
                        DOB = c.DateTime(nullable: false),
                        MaritalStatus = c.String(),
                        Religion = c.Int(nullable: false),
                        Nationality = c.String(),
                        NID = c.String(),
                        PresentAddress = c.String(),
                        PermanentAddress = c.String(),
                        Phone = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Image = c.Binary(),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.EmploymentHistories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SchoolName = c.String(nullable: false),
                        SchoolLocation = c.String(),
                        Designation = c.String(nullable: false),
                        From = c.DateTime(nullable: false),
                        To = c.DateTime(nullable: false),
                        EmployeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.JobInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DesignationId = c.Int(nullable: false),
                        DOJ = c.DateTime(nullable: false),
                        Salary = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalLeave = c.Int(nullable: false),
                        Appointment = c.Binary(),
                        AppointmentExt = c.String(),
                        EmployeeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Designations", t => t.DesignationId, cascadeDelete: true)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.DesignationId)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.ExamTitles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TitleName = c.String(nullable: false),
                        EducationLevelId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EducationLevels", t => t.EducationLevelId, cascadeDelete: false)
                .Index(t => t.EducationLevelId);
            
            CreateTable(
                "dbo.ExamMarks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Theory = c.Single(nullable: false),
                        Mcq = c.Single(nullable: false),
                        Practical = c.Single(nullable: false),
                        Total = c.Single(nullable: false),
                        Grade = c.String(),
                        SubjectId = c.Int(nullable: false),
                        SessionId = c.Int(nullable: false),
                        StudentClassId = c.Int(nullable: false),
                        AssignRollId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AssignRolls", t => t.AssignRollId, cascadeDelete: true)
                .ForeignKey("dbo.Sessions", t => t.SessionId, cascadeDelete: false)
                .ForeignKey("dbo.StudentClasses", t => t.StudentClassId, cascadeDelete:false)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .Index(t => t.SubjectId)
                .Index(t => t.SessionId)
                .Index(t => t.StudentClassId)
                .Index(t => t.AssignRollId);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Code = c.String(nullable: false),
                        Subject_Credit = c.Double(nullable: false),
                        SubjectAssignTo = c.String(),
                        Theory = c.Int(nullable: false),
                        Mcq = c.Int(nullable: false),
                        Practical = c.Int(nullable: false),
                        Total = c.Int(nullable: false),
                        StudentClassId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StudentClasses", t => t.StudentClassId, cascadeDelete: false)
                .Index(t => t.StudentClassId);
            
            CreateTable(
                "dbo.FeeTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Grades",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        LowestMark = c.Int(nullable: false),
                        HighestMark = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Guardians",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Phone = c.Int(nullable: false),
                        Email = c.String(),
                        NID = c.String(nullable: false),
                        GuardianTypeId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GuardianTypes", t => t.GuardianTypeId, cascadeDelete: true)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .Index(t => t.GuardianTypeId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.GuardianTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Registrations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SessionId = c.Int(nullable: false),
                        StudentClassId = c.Int(nullable: false),
                        StudentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sessions", t => t.SessionId, cascadeDelete: true)
                .ForeignKey("dbo.Students", t => t.StudentId, cascadeDelete: true)
                .ForeignKey("dbo.StudentClasses", t => t.StudentClassId, cascadeDelete: true)
                .Index(t => t.SessionId)
                .Index(t => t.StudentClassId)
                .Index(t => t.StudentId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Schools",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Address = c.String(nullable: false),
                        PhonePrimary = c.String(nullable: false),
                        PhoneAlt = c.String(),
                        Fax = c.String(),
                        Email = c.String(),
                        Logo = c.Binary(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.SubjectAssignToTeachers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StudentClassId = c.Int(nullable: false),
                        TeacherId = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StudentClasses", t => t.StudentClassId, cascadeDelete: true)
                .ForeignKey("dbo.Subjects", t => t.SubjectId, cascadeDelete: true)
                .ForeignKey("dbo.Teachers", t => t.TeacherId, cascadeDelete: true)
                .Index(t => t.StudentClassId)
                .Index(t => t.TeacherId)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SubjectAssignToTeachers", "TeacherId", "dbo.Teachers");
            DropForeignKey("dbo.SubjectAssignToTeachers", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.SubjectAssignToTeachers", "StudentClassId", "dbo.StudentClasses");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Registrations", "StudentClassId", "dbo.StudentClasses");
            DropForeignKey("dbo.Registrations", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Registrations", "SessionId", "dbo.Sessions");
            DropForeignKey("dbo.Guardians", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Guardians", "GuardianTypeId", "dbo.GuardianTypes");
            DropForeignKey("dbo.ExamMarks", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Subjects", "StudentClassId", "dbo.StudentClasses");
            DropForeignKey("dbo.ExamMarks", "StudentClassId", "dbo.StudentClasses");
            DropForeignKey("dbo.ExamMarks", "SessionId", "dbo.Sessions");
            DropForeignKey("dbo.ExamMarks", "AssignRollId", "dbo.AssignRolls");
            DropForeignKey("dbo.EmployeeEducations", "ExamTitleId", "dbo.ExamTitles");
            DropForeignKey("dbo.ExamTitles", "EducationLevelId", "dbo.EducationLevels");
            DropForeignKey("dbo.JobInfoes", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.JobInfoes", "DesignationId", "dbo.Designations");
            DropForeignKey("dbo.EmploymentHistories", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.EmployeeEducations", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.EmployeeEducations", "EducationLevelId", "dbo.EducationLevels");
            DropForeignKey("dbo.AssignTeacheToClasses", "TeacherId", "dbo.Teachers");
            DropForeignKey("dbo.Teachers", "StudentClassId", "dbo.StudentClasses");
            DropForeignKey("dbo.Teachers", "DesignationId", "dbo.Designations");
            DropForeignKey("dbo.AssignTeacheToClasses", "StudentClassId", "dbo.StudentClasses");
            DropForeignKey("dbo.AssignRolls", "StudentClassId", "dbo.StudentClasses");
            DropForeignKey("dbo.AssignRolls", "StudentId", "dbo.Students");
            DropForeignKey("dbo.AssignRolls", "SessionId", "dbo.Sessions");
            DropForeignKey("dbo.AprovedApplications", "ClassNameId", "dbo.ClassNames");
            DropForeignKey("dbo.ApplicationDocuments", "StudentApplicationId", "dbo.StudentApplications");
            DropForeignKey("dbo.StudentApplications", "ClassNameId", "dbo.ClassNames");
            DropForeignKey("dbo.Admissions", "StudentClassId", "dbo.StudentClasses");
            DropForeignKey("dbo.StudentClasses", "ShiftId", "dbo.Shifts");
            DropForeignKey("dbo.StudentClasses", "SectionId", "dbo.Sections");
            DropForeignKey("dbo.StudentClasses", "ClassNameId", "dbo.ClassNames");
            DropForeignKey("dbo.Admissions", "StudentId", "dbo.Students");
            DropForeignKey("dbo.Admissions", "SessionId", "dbo.Sessions");
            DropForeignKey("dbo.Admissions", "GroupId", "dbo.Groups");
            DropForeignKey("dbo.Admissions", "ClassFeeId", "dbo.ClassFees");
            DropForeignKey("dbo.ClassFees", "ClassNameId", "dbo.ClassNames");
            DropForeignKey("dbo.AccountLists", "AccountGroupId", "dbo.AccountGroups");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.SubjectAssignToTeachers", new[] { "SubjectId" });
            DropIndex("dbo.SubjectAssignToTeachers", new[] { "TeacherId" });
            DropIndex("dbo.SubjectAssignToTeachers", new[] { "StudentClassId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Registrations", new[] { "StudentId" });
            DropIndex("dbo.Registrations", new[] { "StudentClassId" });
            DropIndex("dbo.Registrations", new[] { "SessionId" });
            DropIndex("dbo.Guardians", new[] { "StudentId" });
            DropIndex("dbo.Guardians", new[] { "GuardianTypeId" });
            DropIndex("dbo.Subjects", new[] { "StudentClassId" });
            DropIndex("dbo.ExamMarks", new[] { "AssignRollId" });
            DropIndex("dbo.ExamMarks", new[] { "StudentClassId" });
            DropIndex("dbo.ExamMarks", new[] { "SessionId" });
            DropIndex("dbo.ExamMarks", new[] { "SubjectId" });
            DropIndex("dbo.ExamTitles", new[] { "EducationLevelId" });
            DropIndex("dbo.JobInfoes", new[] { "EmployeeId" });
            DropIndex("dbo.JobInfoes", new[] { "DesignationId" });
            DropIndex("dbo.EmploymentHistories", new[] { "EmployeeId" });
            DropIndex("dbo.EmployeeEducations", new[] { "EmployeeId" });
            DropIndex("dbo.EmployeeEducations", new[] { "ExamTitleId" });
            DropIndex("dbo.EmployeeEducations", new[] { "EducationLevelId" });
            DropIndex("dbo.Teachers", new[] { "StudentClassId" });
            DropIndex("dbo.Teachers", new[] { "DesignationId" });
            DropIndex("dbo.AssignTeacheToClasses", new[] { "StudentClassId" });
            DropIndex("dbo.AssignTeacheToClasses", new[] { "TeacherId" });
            DropIndex("dbo.AssignRolls", new[] { "StudentId" });
            DropIndex("dbo.AssignRolls", new[] { "StudentClassId" });
            DropIndex("dbo.AssignRolls", new[] { "SessionId" });
            DropIndex("dbo.AprovedApplications", new[] { "ClassNameId" });
            DropIndex("dbo.StudentApplications", new[] { "ClassNameId" });
            DropIndex("dbo.ApplicationDocuments", new[] { "StudentApplicationId" });
            DropIndex("dbo.StudentClasses", new[] { "ShiftId" });
            DropIndex("dbo.StudentClasses", new[] { "SectionId" });
            DropIndex("dbo.StudentClasses", new[] { "ClassNameId" });
            DropIndex("dbo.ClassFees", new[] { "ClassNameId" });
            DropIndex("dbo.Admissions", new[] { "ClassFeeId" });
            DropIndex("dbo.Admissions", new[] { "StudentId" });
            DropIndex("dbo.Admissions", new[] { "GroupId" });
            DropIndex("dbo.Admissions", new[] { "StudentClassId" });
            DropIndex("dbo.Admissions", new[] { "SessionId" });
            DropIndex("dbo.AccountLists", new[] { "AccountGroupId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.SubjectAssignToTeachers");
            DropTable("dbo.Schools");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Registrations");
            DropTable("dbo.GuardianTypes");
            DropTable("dbo.Guardians");
            DropTable("dbo.Grades");
            DropTable("dbo.FeeTypes");
            DropTable("dbo.Subjects");
            DropTable("dbo.ExamMarks");
            DropTable("dbo.ExamTitles");
            DropTable("dbo.JobInfoes");
            DropTable("dbo.EmploymentHistories");
            DropTable("dbo.Employees");
            DropTable("dbo.EmployeeEducations");
            DropTable("dbo.EducationLevels");
            DropTable("dbo.DefaultSettings");
            DropTable("dbo.Calenders");
            DropTable("dbo.Designations");
            DropTable("dbo.Teachers");
            DropTable("dbo.AssignTeacheToClasses");
            DropTable("dbo.AssignRolls");
            DropTable("dbo.AprovedApplications");
            DropTable("dbo.StudentApplications");
            DropTable("dbo.ApplicationDocuments");
            DropTable("dbo.Shifts");
            DropTable("dbo.Sections");
            DropTable("dbo.StudentClasses");
            DropTable("dbo.Students");
            DropTable("dbo.Sessions");
            DropTable("dbo.Groups");
            DropTable("dbo.ClassNames");
            DropTable("dbo.ClassFees");
            DropTable("dbo.Admissions");
            DropTable("dbo.Admins");
            DropTable("dbo.AccountLists");
            DropTable("dbo.AccountGroups");
        }
    }
}
