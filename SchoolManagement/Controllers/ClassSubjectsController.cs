﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SchoolManagement.Model;
using SchoolManagement.Model.ViewModels;
using System.IO;
using SchoolManagement.Model.Entity;
using SchoolManagement.Models.ViewModels;

namespace SchoolManagement.Controllers
{
    public class ClassSubjectsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: ClassSubjects
        public ActionResult FindSubject()
        {
            var StudentClass = db.StudentClasses.Select(c => new
            {
                Id = c.Id,
                Name = c.ClassName.Name + " || " + c.Shift.Name + " ||" + c.Section.Name
            }).OrderBy(o => o.Name).ToList();
            ViewBag.StudentClassId = new SelectList(StudentClass, "Id", "Name");

            return View();

        }
        [HttpPost]

        public ActionResult FindSubject(int StudentClassId)
        {
            try
            {

                List<SubjectVM> SubjectsList = new List<SubjectVM>();
                List<ClassSubjectVM> ClassSubjectsList = new List<ClassSubjectVM>();


                var className = db.StudentClasses.Where(x => x.Id == StudentClassId).Select(x => x.ClassName.Name).FirstOrDefault();
                #region retrieve & bind SubjectsList
                var Subjectz = db.Subjects.Where(x => x.StudentClassId == StudentClassId)
                    .Select(s => new
                    {
                        Code = s.Code,
                        Name = s.Name,



                    }).ToList();
                foreach (var item in Subjectz)
                {
                    SubjectVM sj = new SubjectVM();

                    sj.ClassSubjectCode = item.Code;
                    sj.ClassSubjectName = item.Name;
                    SubjectsList.Add(sj);
                }
                #endregion
                ClassSubjectVM Sr = new ClassSubjectVM
                {
                    SubjectVM = SubjectsList
                };
                ClassSubjectsList.Add(Sr);


                #region send viewBag
                var studentClass = db.StudentClasses.Select(c => new
                {
                    Id = c.Id,
                    Name = c.ClassName.Name + " || " + c.Shift.Name + " ||" + c.Section.Name
                }).OrderBy(o => o.Name).ToList();
                ViewBag.StudentClassId = new SelectList(studentClass, "Id", "Name");
                #endregion
                return View(ClassSubjectsList);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}




            
    

