﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchoolManagement.Model;
using SchoolManagement.Model.Entity;

namespace SchoolManagement.Controllers
{
    //[Authorize(Roles = "Admin,Employees")]
    public class GuardiansController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        public ActionResult Index()
        {
            var guardian = db.Guardians.Include(g => g.GuardianType).Include(g => g.Student);
            return View(guardian.ToList());
        }


        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guardian guardian = db.Guardians.Find(id);
            if (guardian == null)
            {
                return HttpNotFound();
            }
            return View(guardian);
        }


        public ActionResult Create()
        {
            ViewBag.GuardianTypeId = new SelectList(db.GuardianTypes, "Id", "Name");
            ViewBag.StudentId = new SelectList(db.Students, "Id", "Name");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Phone,Email,NID,GuardianTypeId,StudentId")] Guardian guardian)
        {
            if (ModelState.IsValid)
            {
                db.Guardians.Add(guardian);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GuardianTypeId = new SelectList(db.GuardianTypes, "Id", "Name", guardian.GuardianTypeId);
            ViewBag.StudentId = new SelectList(db.Students, "Id", "Name", guardian.StudentId);
            return View(guardian);
        }


        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guardian guardian = db.Guardians.Find(id);
            if (guardian == null)
            {
                return HttpNotFound();
            }
            ViewBag.GuardianTypeId = new SelectList(db.GuardianTypes, "Id", "Name", guardian.GuardianTypeId);
            ViewBag.StudentId = new SelectList(db.Students, "Id", "Name", guardian.StudentId);
            return View(guardian);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Phone,Email,NID,GuardianTypeId,StudentId")] Guardian guardian)
        {
            if (ModelState.IsValid)
            {
                db.Entry(guardian).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GuardianTypeId = new SelectList(db.GuardianTypes, "Id", "Name", guardian.GuardianTypeId);
            ViewBag.StudentId = new SelectList(db.Students, "Id", "Name", guardian.StudentId);
            return View(guardian);
        }


        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guardian guardian = db.Guardians.Find(id);
            if (guardian == null)
            {
                return HttpNotFound();
            }
            return View(guardian);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Guardian guardian = db.Guardians.Find(id);
            db.Guardians.Remove(guardian);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
