﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using PagedList;
using SchoolManagement.Model;
using SchoolManagement.Model.Entity;

namespace SchoolManagement.Controllers
{
    //[Authorize(Roles = "Admin")]
    public class SubjectController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        //public ActionResult Index()
        //{
        //    var Subject = db.Subjects.Include(c => c.StudentClass);

        //    return View(Subject.ToList());

        //}
        public ActionResult Index(string option, string search, int? pageNumber, string sort)
        {
            var Subject = db.Subjects.Include(c => c.StudentClass).Include(c=>c.StudentClass.Section).Include(c=>c.StudentClass.Shift);
           
               
            if (option == "StudentClass")
            {
                //Index action method will return a view with a student records based on what a user specify the value in textbox  
                return View(Subject.Where(x => x.StudentClass.Name.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 8));
            }
            else if (option == "Code")
            {
                return View(Subject.Where(x => x.Code.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 5));
            }
            else
            {
                    return View(Subject.Where(x => x.Name.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 3));
               
                }

                // return View(Subject.ToList());
                //return View("Index");
            }
        public ActionResult ExportToExcel()
        {
            var gv = new GridView();
            gv.DataSource = db.Studentapplications.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View("Index");
        }


        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject Subject = db.Subjects.Find(id);
            if (Subject == null)
            {
                return HttpNotFound();
            }
            return View(Subject);
        }

        //[HttpPost]
       // [ValidateAntiForgeryToken]
        public ActionResult Create()
        {
            var studentClass = db.StudentClasses.Select(c => new
            {
                Id = c.Id,
                Name = c.ClassName.Name + " || " + c.Shift.Name + " ||" + c.Section.Name
            }).OrderBy(o => o.Name).ToList();
            ViewBag.StudentClassId = new SelectList(studentClass, "Id","Name");

            
            return View();
        }

        // POST: Courses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Code,Theory,Mcq,Practical,StudentClassId")] Subject Subject)
        {

           
            if (ModelState.IsValid)
            {
                
                db.Subjects.Add(Subject);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id", "ClassName.Name", Subject.StudentClassId); ;
            return View(Subject);
        }

        // GET: Courses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject Subject = db.Subjects.Find(id);
            if (Subject == null)
            {
                return HttpNotFound();
            }
            //ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id");
            return View(Subject);
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Subject Subject)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Subject).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id");
            return View(Subject);
        }

        // GET: Courses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Subject Subject = db.Subjects.Find(id);
            if (Subject == null)
            {
                return HttpNotFound();
            }
            return View(Subject);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Subject Subject = db.Subjects.Find(id);
            db.Subjects.Remove(Subject);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
