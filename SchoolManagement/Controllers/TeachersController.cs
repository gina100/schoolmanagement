﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using SchoolManagement.Model;
using SchoolManagement.Models.Entity;

namespace SchoolManagement.Controllers
{
    public class TeachersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Teachers
        public ActionResult Index(string option, string search, int? pageNumber, string sort)
        {
            var teacher = db.Teachers.Include(t => t.StudentClass).Include(t => t.Designation);

            //var studentClass = db.StudentClasses.Select(c => new
            //{
            //    Id = c.Id,
            //    Name = c.ClassName.Name + " || " + c.Shift.Name + " ||" + c.Section.Name
            //}).OrderBy(o => o.Name).ToList();
            if (option == "StudentClass")
            {
                //Index action method will return a view with a student records based on what a user specify the value in textbox  
                return View(teacher.Where(x => x.StudentClass.ClassName.Name.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 8));
            }
            else if (option == "Name")
            {
                return View(teacher.Where(x => x.Name.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 5));
            }
            else
            {
                return View(teacher.Where(x => x.Surname.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 3));

            }

            
        
            //return View(teacher.ToList());
        }

        // GET: Teachers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teacher Teacher = db.Teachers.Find(id);
            if (Teacher == null)
            {
                return HttpNotFound();
            }
            return View(Teacher);
        }

        // GET: Teachers/Create
        [HttpGet]
        public ActionResult Create()
        {
                  var studentClass = db.StudentClasses.Select(c => new
            {
                Id = c.Id,
                Name = c.ClassName.Name + " || " + c.Shift.Name + " ||" + c.Section.Name
            }).OrderBy(o => o.Name).ToList();
            ViewBag.StudentClassId = new SelectList(studentClass, "Id","Name");
            ViewBag.DesignationId = new SelectList(db.Designations, "Id", "Name");
            return View();
        }

        // POST: Teachers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Surname,Gender,Address,Email,PhoneNumber,DesignationId,StudentClassId")] Teacher Teacher)
        {
            if (ModelState.IsValid)
            {
                db.Teachers.Add(Teacher);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id","Name",Teacher.StudentClassId);
            ViewBag.DesignationId = new SelectList(db.Designations, "Id", "Name");
            return View(Teacher);
        }

        // GET: Teachers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teacher Teacher = db.Teachers.Find(id);
            if (Teacher == null)
            {
                return HttpNotFound();
            }
            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id","Name",Teacher.StudentClassId);
            ViewBag.DesignationId = new SelectList(db.Designations, "Id", "Name");
            return View(Teacher);
        }

        // POST: Teachers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Surname,Gender,Address,Email,PhoneNumber,DesignationId,StudentClassId")] Teacher Teacher)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Teacher).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id");
            ViewBag.DesignationId = new SelectList(db.Designations, "Id", "Name");
            return View(Teacher);
        }

        // GET: Teachers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teacher Teacher = db.Teachers.Find(id);
            if (Teacher == null)
            {
                return HttpNotFound();
            }
            return View(Teacher);
        }

        // POST: Teachers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Teacher teacher = db.Teachers.Find(id);
            db.Teachers.Remove(teacher);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
