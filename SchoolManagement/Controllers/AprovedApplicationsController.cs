﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNet.Identity;
using PagedList;
using SchoolManagement.Model;
using SchoolManagement.Model.Entity;
using SchoolManagement.Models.Entity;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SchoolManagement.Controllers
{
    public class AprovedApplicationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AprovedApplications
        //public ActionResult Index()
        //{
        //    var aprovedApplications = db.AprovedApplications.Include(a => a.ClassName);
        //    return View(aprovedApplications.ToList());
        //}
        public ActionResult Index(string option, string search, int? pageNumber, string sort)
        {
            var aprovedApplications = db.AprovedApplications.Include(a => a.ClassName);
            // return View(aprovedApplications.ToList());
           

            //if a user choose the radio button option as Subject  
            if (option == "NID")
            {
                //Index action method will return a view with a student records based on what a user specify the value in textbox  
                return View(aprovedApplications.Where(x => x.NID.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 5));
            }
            else if (option == "Surname")
            {
                return View(aprovedApplications.Where(x => x.Surname.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 5));
            }
            else
            {
                return View(aprovedApplications.Where(x => x.Name.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 3));
            }
        
    }
        public ActionResult ExportToExcel()
        {
            var gv = new GridView();
            gv.DataSource = db.Studentapplications.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View("Index");
        }

            // GET: AprovedApplications/Details/5
            public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AprovedApplications aprovedApplications = db.AprovedApplications.Find(id);
            if (aprovedApplications == null)
            {
                return HttpNotFound();
            }
            return View(aprovedApplications);
        }

        // GET: AprovedApplications/Create
        public ActionResult Create(int id)
        {
            ViewBag.Id = id;
            ViewBag.ClassNameId = new SelectList(db.ClassNames, "Id", "Name");
            return View();
        }

        // POST: AprovedApplications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Surname,FatherName,MotherName,DOB,NID,Gender,HomeLanguage,Race,Email,PresentAddress,ParmanentAddress,Religion,ClassNameId,Status")] AprovedApplications aprovedApplications)
        {
            if (ModelState.IsValid)
            {
               
                db.AprovedApplications.Add(aprovedApplications);
                db.SaveChanges();
                return RedirectToAction("Index");







            }

            ViewBag.ClassNameId = new SelectList(db.ClassNames, "Id", "Name", aprovedApplications.ClassNameId);
            return View(aprovedApplications);
        }

        // GET: AprovedApplications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AprovedApplications aprovedApplications = db.AprovedApplications.Find(id);
            if (aprovedApplications == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassNameId = new SelectList(db.ClassNames, "Id", "Name", aprovedApplications.ClassNameId);
            return View(aprovedApplications);
        }

        // POST: AprovedApplications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Surname,FatherName,MotherName,DOB,NID,Gender,HomeLanguage,Race,Email,PresentAddress,ParmanentAddress,Religion,ClassNameId,Status")] AprovedApplications aprovedApplications)
        {
            if (ModelState.IsValid)
            {

                db.Entry(aprovedApplications).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClassNameId = new SelectList(db.ClassNames, "Id", "Name", aprovedApplications.ClassNameId);
            return View(aprovedApplications);
        }

        // GET: AprovedApplications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AprovedApplications aprovedApplications = db.AprovedApplications.Find(id);
            if (aprovedApplications == null)
            {
                return HttpNotFound();
            }
            return View(aprovedApplications);
        }

        // POST: AprovedApplications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AprovedApplications aprovedApplications = db.AprovedApplications.Find(id);
            db.AprovedApplications.Remove(aprovedApplications);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
