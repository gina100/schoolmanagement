﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.UI;
//using System.Web.UI.WebControls;
using PagedList;
using System.IO;
using System.Web;

using System.Web.Mvc;
using SchoolManagement.Model;
using SchoolManagement.Models.Entity;

namespace SchoolManagement.Controllers
{
    public class SubjectAssignToTeachersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: SubjectAssignToTeachers
        public ActionResult Index()
        {
            var subjectAssignToTeachers = db.SubjectAssignToTeachers.Include(s => s.StudentClass).Include(c=>c.StudentClass.Section).Include(c=>c.StudentClass.Section).Include(s => s.Subject).Include(s => s.Teacher).ToList();
            return View(subjectAssignToTeachers.ToList());
        }
        //public ActionResult Index(String option, string search, int? pageNumber, string sort)
        //{
        //    var subjectAssignToTeachers = db.SubjectAssignToTeachers.Include(s => s.StudentClass).Include(c => c.StudentClass.Section).Include(c => c.StudentClass.Shift).Include(s => s.Subject).Include(s => s.Teacher);
        //    if (option == "Code")
        //    {
        //        //Index action method will return a view with a student records based on what a user specify the value in textbox  
        //        return View(SubjectAssignToTeacher.Where(x => x.Code.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 5));
        //    }
        //    else if (option == "Name")
        //    {
        //        return View(SubjectAssignToTeacher.Where(x => x.Name.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 5));
        //    }
        //    else
        //    {
        //        return View(SubjectAssignToTeacher.Where(x => x.Teacher.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 3));

        //    }

            // return View(Subject.ToList());
            //return View("Index");
        
    

        // GET: SubjectAssignToTeachers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectAssignToTeacher subjectAssignToTeacher = db.SubjectAssignToTeachers.Find(id);
            if (subjectAssignToTeacher == null)
            {
                return HttpNotFound();
            }
            return View(subjectAssignToTeacher);
        }

        // GET: SubjectAssignToTeachers/Create
        public ActionResult Create()
        {
            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id", "ClassName.Name");
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name");
            ViewBag.TeacherId = new SelectList(db.Teachers, "Id", "Name");
            return View();
        }

        // POST: SubjectAssignToTeachers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,StudentClassId,TeacherId,SubjectId")] SubjectAssignToTeacher subjectAssignToTeacher)
        {
            if (ModelState.IsValid)
            {
                if (subjectAssignToTeacher.CheckExists())
                {
                    ModelState.AddModelError("", "You can not assing a subject twice to the same teacher of the same class");
                    ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id", "ClassName.Name", subjectAssignToTeacher.StudentClassId);
                    ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", subjectAssignToTeacher.SubjectId);
                    ViewBag.TeacherId = new SelectList(db.Teachers, "Id", "Name", subjectAssignToTeacher.TeacherId);
                    return View(subjectAssignToTeacher);
                    
                }
                else
                {
                    db.SubjectAssignToTeachers.Add(subjectAssignToTeacher);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }

            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id", "ClassName.Name", subjectAssignToTeacher.StudentClassId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", subjectAssignToTeacher.SubjectId);
            ViewBag.TeacherId = new SelectList(db.Teachers, "Id", "Name", subjectAssignToTeacher.TeacherId);
            return View(subjectAssignToTeacher);
        }

        // GET: SubjectAssignToTeachers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectAssignToTeacher subjectAssignToTeacher = db.SubjectAssignToTeachers.Find(id);
            if (subjectAssignToTeacher == null)
            {
                return HttpNotFound();
            }
            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id", "Id", subjectAssignToTeacher.StudentClassId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", subjectAssignToTeacher.SubjectId);
            ViewBag.TeacherId = new SelectList(db.Teachers, "Id", "Name", subjectAssignToTeacher.TeacherId);
            return View(subjectAssignToTeacher);
        }

        // POST: SubjectAssignToTeachers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,StudentClassId,TeacherId,SubjectId")] SubjectAssignToTeacher subjectAssignToTeacher)
        {
            if (ModelState.IsValid)
            {
                db.Entry(subjectAssignToTeacher).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id", "Id", subjectAssignToTeacher.StudentClassId);
            ViewBag.SubjectId = new SelectList(db.Subjects, "Id", "Name", subjectAssignToTeacher.SubjectId);
            ViewBag.TeacherId = new SelectList(db.Teachers, "Id", "Name", subjectAssignToTeacher.TeacherId);
            return View(subjectAssignToTeacher);
        }

        // GET: SubjectAssignToTeachers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SubjectAssignToTeacher subjectAssignToTeacher = db.SubjectAssignToTeachers.Find(id);
            if (subjectAssignToTeacher == null)
            {
                return HttpNotFound();
            }
            return View(subjectAssignToTeacher);
        }

        // POST: SubjectAssignToTeachers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SubjectAssignToTeacher subjectAssignToTeacher = db.SubjectAssignToTeachers.Find(id);
            db.SubjectAssignToTeachers.Remove(subjectAssignToTeacher);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
