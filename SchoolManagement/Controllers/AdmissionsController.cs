﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchoolManagement.Model;
using SchoolManagement.Model.Entity;

namespace SchoolManagement.Controllers
{
    public class AdmissionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admissions
        public ActionResult Index()
        {
            var admissions = db.Admissions.Include(a => a.ClassFee).Include(a => a.Group).Include(a => a.Session).Include(a => a.Student).Include(a => a.StudentClass);
            return View(admissions.ToList());
        }

        // GET: Admissions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Admission admission = db.Admissions.Find(id);
            if (admission == null)
            {
                return HttpNotFound();
            }
            return View(admission);
        }

        // GET: Admissions/Create
        public ActionResult Create()
        {
            ViewBag.ClassFeeId = new SelectList(db.ClassFees, "Id", "Fee");
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name");
            ViewBag.SessionId = new SelectList(db.Sessions, "Id", "Name");
            ViewBag.StudentId = new SelectList(db.Students, "Id", "Name");
            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id","ClassName.Name");
            return View();
        }

        // POST: Admissions/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,AdmissionDate,PreviousSchool,PreviousSchoolAddrs,PreviousSchoolDocument,Extension,SessionId,StudentClassId,GroupId,StudentId,ClassFeeId")] Admission admission)
        {
            if (ModelState.IsValid)
            {
                db.Admissions.Add(admission);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ClassFeeId = new SelectList(db.ClassFees, "Id", "Fee", admission.ClassFeeId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", admission.GroupId);
            ViewBag.SessionId = new SelectList(db.Sessions, "Id", "Name", admission.SessionId);
            ViewBag.StudentId = new SelectList(db.Students, "Id", "Name", admission.StudentId);
            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id", "ClassName.Name", admission.StudentClassId);
            return View(admission);
        }

        // GET: Admissions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Admission admission = db.Admissions.Find(id);
            if (admission == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassFeeId = new SelectList(db.ClassFees, "Id", "Id", admission.ClassFeeId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", admission.GroupId);
            ViewBag.SessionId = new SelectList(db.Sessions, "Id", "Name", admission.SessionId);
            ViewBag.StudentId = new SelectList(db.Students, "Id", "Name", admission.StudentId);
            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id", "Id", admission.StudentClassId);
            return View(admission);
        }

        // POST: Admissions/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,AdmissionDate,PreviousSchool,PreviousSchoolAddrs,PreviousSchoolDocument,Extension,SessionId,StudentClassId,GroupId,StudentId,ClassFeeId")] Admission admission)
        {
            if (ModelState.IsValid)
            {
                db.Entry(admission).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClassFeeId = new SelectList(db.ClassFees, "Id", "Id", admission.ClassFeeId);
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name", admission.GroupId);
            ViewBag.SessionId = new SelectList(db.Sessions, "Id", "Name", admission.SessionId);
            ViewBag.StudentId = new SelectList(db.Students, "Id", "Name", admission.StudentId);
            ViewBag.StudentClassId = new SelectList(db.StudentClasses, "Id", "Id", admission.StudentClassId);
            return View(admission);
        }

        // GET: Admissions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Admission admission = db.Admissions.Find(id);
            if (admission == null)
            {
                return HttpNotFound();
            }
            return View(admission);
        }

        // POST: Admissions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Admission admission = db.Admissions.Find(id);
            db.Admissions.Remove(admission);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
