﻿using SchoolManagement.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
    public class CommonController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public JsonResult GetStudent(int session, int studentClass)
        {
            var data = db.Admissions.Where(x => x.SessionId == session && x.StudentClassId == studentClass).Select(s => new
            {
                Id = s.Student.Id,
                Name = s.Student.Name
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRoll(int session, int studentClass)
        {
            var data = db.AssignRolls.Where(x => x.SessionId == session && x.StudentClassId == studentClass).Select(s => new
            {
                Id = s.Id,
                Roll = s.Roll + "||" + s.Student.Name
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubject(int studentClass)
        {
            var classNameId = db.StudentClasses.Where(x => x.Id == studentClass).Select(s => s.ClassNameId).FirstOrDefault();
            var data = db.Subjects.Where(x => x.StudentClassId == classNameId).Select(s => new
            {
                Id = s.Id,
                Name = s.Code + "||" + s.Name
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetExamTitle(int EducationLevel)
        {
            var data = db.ExamTitles.Where(x => x.EducationLevelId == EducationLevel).Select(s => new
            {
                Id = s.Id,
                Name = s.TitleName
            }).ToList();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}