﻿using SchoolManagement.Model;
using SchoolManagement.Helper;
using SchoolManagement.Model.Entity;
using SchoolManagement.Models.Entity;
using SchoolManagement.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagement.Controllers
{
   // [Authorize(Roles = "Admin")]
    public class AdmisionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private Appfunction ap = new Appfunction();

        public ActionResult Create()
        {
            var studentClass = db.StudentClasses.Select(c => new
            {
                Id = c.Id,
                Name = c.ClassName.Name + " || " + c.Shift.Name + " ||" + c.Section.Name
            }).OrderBy(o => o.Name).ToList();

            var classFee = db.ClassFees.Select(c => new
            {
                Id = c.Id,
                Name = c.ClassName.Name + " || " + c.AdmissionFee
            }).OrderBy(o => o.Name).ToList();

            ViewBag.SessionId = new SelectList(db.Sessions, "Id", "Name");
            ViewBag.GuardianTypeId = new SelectList(db.GuardianTypes, "Id", "Name");
            ViewBag.StudentClassId = new SelectList(studentClass, "Id", "Name");
            ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name");
            ViewBag.ClassFeeId = new SelectList(classFee, "Id", "Name");
            return View();
        }

        [HttpPost]
        public ActionResult Create(AdmissionVM viewModel, HttpPostedFileBase StudentImage, HttpPostedFileBase Document)
        {
            using (var dbTransaction = db.Database.BeginTransaction())
            {
                try
                {
                    #region validation
                    //if (!ModelState.IsValid)
                    //{
                    //    var studentClass = db.StudentClass.Select(c => new
                    //    {
                    //        Id = c.Id,
                    //        Name = c.ClassName.Name + " || " + c.Shift.Name + " ||" + c.Section.Name
                    //    }).OrderBy(o => o.Name).ToList();

                    //    ViewBag.SessionId = new SelectList(db.Session, "Id", "Name");
                    //    ViewBag.GuardianTypeId = new SelectList(db.GuardianType, "Id", "Name");
                    //    ViewBag.StudentClassId = new SelectList(studentClass, "Id", "Name");
                    //    ViewBag.GroupId = new SelectList(db.Group, "Id", "Name");
                    //    return View();
                    //}
                    #endregion

                    #region Create Students

                    var student = new Student
                    {
                        Name = viewModel.StudentName,
                        Surname=viewModel.Surname,
                        FatherName = viewModel.FatherName,
                        MotherName = viewModel.MotherName,
                        DateOfBirth = viewModel.DateOfBirth,
                        Email = viewModel.StudentEmail,
                        PresentAddress = viewModel.PresentAddress,
                        ParmanentAddress = viewModel.ParmanentAddress,
                        Religion = viewModel.Religion,
                        Gender = viewModel.Gender

                    };
                    if (StudentImage != null && StudentImage.ContentLength > 0)
                    {
                        using (var reader = new System.IO.BinaryReader(StudentImage.InputStream))
                        {
                            student.Image = reader.ReadBytes(StudentImage.ContentLength);
                        }

                    }
                    db.Students.Add(student);
                    db.SaveChanges();

                    int studentId = student.Id;
                    #endregion

                    #region Create Guardian
                    if (viewModel.GuardianName != null)
                    {
                        var guardian = new Guardian
                        {
                            Name = viewModel.GuardianName,
                            Email = viewModel.GuardianEmail,
                            Phone = viewModel.GuardianPhone,
                            NID = viewModel.NID,
                            GuardianTypeId = viewModel.GuardianTypeId,
                            StudentId = studentId
                        };

                        db.Guardians.Add(guardian);
                        db.SaveChanges();
                    }

                    #endregion

                    #region Create Admission
                    if (viewModel.SessionId != 0)
                    {
                        var admission = new Admission
                        {
                            AdmissionDate = viewModel.AdmissionDate,
                            SessionId = viewModel.SessionId,
                            PreviousSchool = viewModel.PreviousSchool,
                            PreviousSchoolAddrs = viewModel.PreviousSchoolAddrs,
                            StudentClassId = viewModel.StudentClassId,
                            GroupId = viewModel.GroupId,
                            StudentId = studentId
                        };
                        if (Document != null && Document.ContentLength > 0)
                        {
                            using (var reader = new System.IO.BinaryReader(Document.InputStream))
                            {
                                admission.PreviousSchoolDocument = reader.ReadBytes(Document.ContentLength);
                            }

                        }

                        db.Admissions.Add(admission);
                        db.SaveChanges();
                    }

                    #endregion

                    #region update Admission Account
                    if (viewModel.ClassFeeId != 0)
                    {
                        var amount = db.ClassFees.Where(i => i.Id == viewModel.ClassFeeId).Select(a => a.AdmissionFee).FirstOrDefault();
                        var prevBalance = db.AccountLists.Where(n => n.Name == "Admission").Select(c => c.CurrentBalance).FirstOrDefault();
                        var newBalance = prevBalance + amount;
                        ap.UpdateAccountListBalance(1000, newBalance);
                    }

                    #endregion

                    dbTransaction.Commit();
                    return RedirectToAction("Index", "Students");
                }
                catch (Exception ex)
                {
                    #region catch
                    string abc = ex.Message + ex.InnerException;
                    dbTransaction.Rollback();

                    var studentClass = db.StudentClasses.Select(c => new
                    {
                        Id = c.Id,
                        Name = c.ClassName.Name + " || " + c.Shift.Name + " ||" + c.Section.Name
                    }).OrderBy(o => o.Name).ToList();
                    ViewBag.SessionId = new SelectList(db.Sessions, "Id", "Name");
                    ViewBag.GuardianTypeId = new SelectList(db.GuardianTypes, "Id", "Name");
                    ViewBag.StudentClassId = new SelectList(studentClass, "Id", "Name");
                    ViewBag.GroupId = new SelectList(db.Groups, "Id", "Name");
                    return RedirectToAction("Create");
                    #endregion
                }
            }
        }


    }
}




