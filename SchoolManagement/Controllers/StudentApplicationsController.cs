﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchoolManagement.Model;
using Microsoft.AspNet.Identity;
using SchoolManagement.Model.Entity;
using PagedList;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using SchoolManagement.Models.Entity;

namespace SchoolManagement.Controllers
{
    public class StudentApplicationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: StudentApplications
        public ActionResult Index2()
        {
            var studentapplications = db.Studentapplications.Include(s => s.ClassName);
            return View(studentapplications.ToList());
        }

        //the first parameter is the option that we choose and the second parameter will use the textbox value  
        public ActionResult Index(string option, string search,int? pageNumber, string sort)
        {
            var studentapplications = db.Studentapplications.Include(s => s.ClassName);

            //if a user choose the radio button option as Subject  
            if (option == "Surname")
            {
                //Index action method will return a view with a student records based on what a user specify the value in textbox  
                return View(studentapplications.Where(x=>x.Surname.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 5));
            }
            else if (option == "Status")
            {
                return View(studentapplications.Where(x => x.Status.StartsWith(search)|| search == null).ToList().ToPagedList(pageNumber ?? 1, 5));
            }
            else
            {
                return View(studentapplications.Where(x => x.Name.StartsWith(search) || search == null).ToList().ToPagedList(pageNumber ?? 1, 3));
            }
        }
        public ActionResult ExportToExcel()
        {
            var gv = new GridView();
            gv.DataSource = db.Studentapplications.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            return View("Index");
        }
        // GET: StudentApplications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentApplication studentApplication = db.Studentapplications.Find(id);
            if (studentApplication == null)
            {
                return HttpNotFound();
            }
            return View(studentApplication);
        }
        public ActionResult ApproveApplication(int? id)
        {    
            StudentApplication studentApplication = db.Studentapplications.Find(id);
            if (studentApplication.Status == "Accepted" || studentApplication.Status == "Rejected")
            {
                TempData["AlertMessage"] = "The application has already been Rejected/Accepted";
                return RedirectToAction("Index");
            }
            else

            {
               





studentApplication.Status = "Accepted";
            

                db.Entry(studentApplication).State = EntityState.Modified;
              // db.Studentapplications.Add(studentApplication);
               db.SaveChanges();
                return RedirectToAction("Index");
            
               
            }

        }
        public ActionResult RejectApplication(int? id)
        {
            StudentApplication studentApplication = db.Studentapplications.Find(id);
            if (studentApplication.Status == "Accepted" || studentApplication.Status == "Rejected")
            {
                TempData["AlertMessage"] = "The application has already been Rejected/Accepted";
                return RedirectToAction("Index");
            }
            else
            {
                studentApplication.Status = "Rejected";
                db.Entry(studentApplication).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        // GET: StudentApplications/Create
        public ActionResult Create()
        {
            ViewBag.ClassNameId = new SelectList(db.StudentClasses, "Id", "Name");
            return View();
        }

        // POST: StudentApplications/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Surname,FatherName,MotherName,DOB,NID,Gender,HomeLanguage,Race,Email,PresentAddress,ParmanentAddress,Religion,ClassNameId")] StudentApplication studentApplication)
        {
            if (ModelState.IsValid)
            {
                studentApplication.Status = "Pending";
                db.Studentapplications.Add(studentApplication);
                db.SaveChanges();
                return RedirectToAction("Create", "ApplicationDocuments", new { id = studentApplication.Id });
            }
            else
            {
                
                ViewBag.ClassNameId = new SelectList(db.StudentClasses, "Id", "Name");

                return View(studentApplication);
            }
        }

        // GET: StudentApplications/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentApplication studentApplication = db.Studentapplications.Find(id);
            if (studentApplication == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClassNameId = new SelectList(db.StudentClasses, "Id", "Id", studentApplication.ClassNameId);
            return View(studentApplication);
        }

        // POST: StudentApplications/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Surname,FatherName,MotherName,DOB,NID,Gender,HomeLanguage,Race,Email,PresentAddress,ParmanentAddress,Religion,ClassNameId")] StudentApplication studentApplication)
        {
            if (ModelState.IsValid)
            {
                db.Entry(studentApplication).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClassNameId = new SelectList(db.StudentClasses, "Id", "Id", studentApplication.ClassNameId);
            return View(studentApplication);
        }

        // GET: StudentApplications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentApplication studentApplication = db.Studentapplications.Find(id);
            if (studentApplication == null)
            {
                return HttpNotFound();
            }
            return View(studentApplication);
        }

        // POST: StudentApplications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StudentApplication studentApplication = db.Studentapplications.Find(id);
            db.Studentapplications.Remove(studentApplication);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
