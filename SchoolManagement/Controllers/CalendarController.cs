﻿using DHTMLX.Common;
using DHTMLX.Scheduler;
using DHTMLX.Scheduler.Data;
using System;
using System.Collections.Generic;
//using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SchoolManagement.Model.Entity;
using SchoolManagement.Model;

namespace SchoolManagement.Controllers
{
    public class CalenderController : Controller
    {
        // GET: Calender
        public ActionResult Index()
        {
            var sched = new DHXScheduler(this);
            sched.Skin = DHXScheduler.Skins.Terrace;
            sched.LoadData = true;
            sched.EnableDataprocessor = true;
            sched.InitialDate = new DateTime(2020, 7, 6);
            return View(sched);
        }
        public ContentResult Data()
        {
            return (new SchedulerAjaxData(
                new ApplicationDbContext().Calenders
                .Select(e => new { e.id, e.Name, e.start_date, e.end_date })
                )
                );
        }
        public ContentResult Save(int? id, FormCollection actionValues)
        {
            var action = new DataAction(actionValues);
            var changedCalender = DHXEventsHelper.Bind<Calender>(actionValues);
            var entities = new ApplicationDbContext();
            try
            {
                switch (action.Type)
                {
                    case DataActionTypes.Insert:
                        entities.Calenders.Add(changedCalender);
                        break;
                    case DataActionTypes.Delete:
                        changedCalender = entities.Calenders.FirstOrDefault(ev => ev.id == action.SourceId);
                        entities.Calenders.Remove(changedCalender);
                        break;
                    default:// "update"
                        var target = entities.Calenders.Single(e => e.id == changedCalender.id);
                        DHXEventsHelper.Update(target, changedCalender, new List<string> { "id" });
                        break;
                }
                entities.SaveChanges();
                action.TargetId = changedCalender.id;
            }
            catch (Exception)
            {
                action.Type = DataActionTypes.Error;
            }

            return (new AjaxSaveResponse(action));
        }
    }
}
