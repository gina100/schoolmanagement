﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

using SchoolManagement.Model;
using SchoolManagement.Model.Entity;

namespace SchoolManagement.Controllers
{
   // [Authorize(Roles = "Admin, Employees")]
    public class AppAccountController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var accountList = db.AccountLists.Include(a => a.AccountGroup);
            return View(accountList.ToList());
        }


        public ActionResult Create()
        {
            ViewBag.AccountGroupId = new SelectList(db.AccountGroups, "Id", "Name");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AccountList accountList)
        {
            if (ModelState.IsValid)
            {
                db.AccountLists.Add(accountList);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AccountGroupId = new SelectList(db.AccountGroups, "Id", "Name", accountList.AccountGroupId);
            return View(accountList);
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountList accountList = db.AccountLists.Find(id);
            if (accountList == null)
            {
                return HttpNotFound();
            }
            ViewBag.AccountGroupId = new SelectList(db.AccountGroups, "Id", "Name", accountList.AccountGroupId);
            return View(accountList);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( AccountList accountList)
        {
            if (ModelState.IsValid)
            {
                db.Entry(accountList).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AccountGroupId = new SelectList(db.AccountGroups, "Id", "Name", accountList.AccountGroupId);
            return View(accountList);
        }

        // GET: AppAccount/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountList accountList = db.AccountLists.Find(id);
            if (accountList == null)
            {
                return HttpNotFound();
            }
            return View(accountList);
        }

        // POST: AppAccount/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AccountList accountList = db.AccountLists.Find(id);
            db.AccountLists.Remove(accountList);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AccountList accountList = db.AccountLists.Find(id);
            if (accountList == null)
            {
                return HttpNotFound();
            }
            return View(accountList);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}